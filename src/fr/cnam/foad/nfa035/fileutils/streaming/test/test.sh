#!/bin/sh

JAVA_PATH="/c/Program Files/Java/jdk-17/bin/java"
PROJECT_PATH=/c/Users/tlombardo/exercice-3

cp $PROJECT_PATH/petite_image_2.png ./
cp $PROJECT_PATH/src/log4j2.xml out/production/exercice-3/
"$JAVA_PATH" -ea -classpath "out/production/exercice-3;src;lib\commons-codec-1.15.jar;lib\log4j-core-2.20.0.jar;lib\log4j-api-2.20.0.jar" -Duser.dir=$PROJECT_PATH fr.cnam.foad.nfa035.fileutils.streaming.test.StreamingTest
rm petite_image_2.png
