package fr.cnam.foad.nfa035.fileutils.streaming.test;

import fr.cnam.foad.nfa035.fileutils.streaming.media.ImageByteArrayFrame;
import fr.cnam.foad.nfa035.fileutils.streaming.serializer.ImageDeserializerBase64StreamingImpl;
import fr.cnam.foad.nfa035.fileutils.streaming.serializer.ImageSerializerBase64StreamingImpl;
import fr.cnam.foad.nfa035.fileutils.streaming.serializer.ImageStreamingDeserializer;
import fr.cnam.foad.nfa035.fileutils.streaming.serializer.ImageStreamingSerializer;
import org.apache.commons.codec.binary.StringUtils;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.TestMethodOrder;
import java.io.*;
import java.nio.file.Files;
import java.util.Arrays;


public class StreamingTest {
    private static final Logger logger = LogManager.getLogger(StreamingTest.class);
    private File image = new File("petite_image_2.png");
    private ImageByteArrayFrame media = new ImageByteArrayFrame(new ByteArrayOutputStream());
    private byte[] originImage;
    private byte[] deserializedImage;

    @Test
    @Order(1)
    @DisplayName("Test de sérialisation")
    public void testSerialization() throws IOException {
        logger.info("Début du test de sérialisation");
        ImageStreamingSerializer serializer = new ImageSerializerBase64StreamingImpl();
        serializer.serialize(image, media);
        String encodedImage = media.getEncodedImageOutput().toString();
        logger.info(encodedImage + "\n");
    }

    @Test
    @Order(2)
    @DisplayName("Test de désérialisation")
    public void testDeserialization() throws IOException {
        logger.info("Début du test de désérialisation");
        ByteArrayOutputStream deserializationOutput = new ByteArrayOutputStream();
        ImageStreamingDeserializer deserializer = new ImageDeserializerBase64StreamingImpl(deserializationOutput);
        deserializer.deserialize(media);
        deserializedImage = ((ByteArrayOutputStream) deserializer.getSourceOutputStream()).toByteArray();
        logger.info("Image désérialisée, taille : " + deserializedImage.length);
        originImage = Files.readAllBytes(image.toPath()); // Initialisation de originImage
        logger.info("Image d'origine, taille : " + originImage.length);
    }


    @Test
    @Order(3)
    @DisplayName("comparaison méthode difference")
    public void testDifference() throws IOException {
        assertNotNull(originImage, "L'image d'origine ne devrait pas être null");

        String originalImageString = new String(originImage);
        String deserializedImageString = new String(deserializedImage);
        int index = indexOfDifference(originalImageString, deserializedImageString);
        logger.info("Index of difference: " + index);
        logger.info("Difference: " + difference(originalImageString, deserializedImageString));
    }

    @Test
    @Order(4)
    @DisplayName("Assert")
    public void testAssert() throws IOException {
        assertArrayEquals(originImage, deserializedImage);
        logger.info("Cette sérialisation est bien réversible :)");
    }

    public static String difference(String str1, String str2) {
        if (str1 == null) {
            return str2;
        }
        if (str2 == null) {
            return str1;
        }
        int at = indexOfDifference(str1, str2);
        if (at == -1) {
            return "";
        }
        return str2.substring(at);
    }

    public static int indexOfDifference(CharSequence cs1, CharSequence cs2) {
        if (cs1 == cs2) {
            return -1;
        }
        if (cs1 == null || cs2 == null) {
            return 0;
        }
        int i;
        for (i = 0; i < cs1.length() && i < cs2.length(); ++i) {
            if (cs1.charAt(i) != cs2.charAt(i)) {
                break;
            }
        }
        if (i < cs2.length() || i < cs1.length()) {
            return i;
        }
        return -1;
    }
}
